package quizapp.icarasia.com.quizapp.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import quizapp.icarasia.com.quizapp.Login.LoginFragment;
import quizapp.icarasia.com.quizapp.SignUp.SignUpFragment;

/**
 * Created by yasirsiddique on 25/09/2016.
 */

/**
 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class SectionsPagerAdapter extends FragmentPagerAdapter {

    int LoginPage = 0;
    int SignUpPage = 1;
    public SectionsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        return (position == LoginPage) ? new LoginFragment() : new SignUpFragment();
    }

    @Override
    public int getCount() {
        // Show 2 total pages.
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Login";
            case 1:
                return "Sign Up";
        }
        return null;
    }
}
