package quizapp.icarasia.com.quizapp.Presenter.ActivityPresenters;

import android.app.Activity;
import android.content.Intent;

import quizapp.icarasia.com.quizapp.Generic.FormValidator;
import quizapp.icarasia.com.quizapp.Model.BasicUser;
import quizapp.icarasia.com.quizapp.Service.UserService;
import quizapp.icarasia.com.quizapp.Utils.QuizAppConstants;
import quizapp.icarasia.com.quizapp.Utils.QuizAppUIUtils;
import quizapp.icarasia.com.quizapp.View.EditMobileNumDialogActivity;

/**
 * Created by yasirsiddique on 25/09/2016.
 */
public class EditMobileNumPresenter {
    private String initialMobileNum = "";
    private EditMobileNumDialogActivity activity;

    public EditMobileNumPresenter (EditMobileNumDialogActivity activity)
    {
        this.activity = activity;
    }

    public String getMobileNumber ()
    {
        BasicUser loggedUser = UserService.getInstance().getLoggedInUser();
        initialMobileNum = loggedUser.getPhoneNumber();
        return initialMobileNum;
    }

    private boolean isNumberChanged (String mobileNumberEntered)
    {
        return (!initialMobileNum.equals(mobileNumberEntered));
    }


    private void finishActivityWithResult ()
    {
        boolean isUserUpdated = false;
        String mobileNum = activity.getMobileNumberEntered();
        if (isNumberChanged(mobileNum))
        {
            isUserUpdated = UserService.getInstance().updateUser(mobileNum);

            if (isUserUpdated)
            {
                moveToPreviousActivity (true);
            }
            else
            {
                activity.showLoginFailure();
            }
        }
        else
        {
            moveToPreviousActivity(false);
        }
    }

    public void actionsSaveButtonClick()
    {
        boolean isValid = FormValidator.isValidCellPhone(activity.getMobileNumberEntered(), QuizAppConstants.PhoneNumberLength);
        if (isValid)
        {
            finishActivityWithResult ();
        }
        else
        {
            activity.showInvalidMobileNumErr();
        }
    }

    private void moveToPreviousActivity (boolean isMobileNumChanged)
    {
        QuizAppUIUtils.hideSoftKeyboard(activity);
        Intent data = new Intent();
        data.putExtra(QuizAppConstants.MobileNumResultKey, isMobileNumChanged);

        if (activity.getParent() == null) {
            activity.setResult(Activity.RESULT_OK, data);
        } else {
            activity.getParent().setResult(Activity.RESULT_OK, data);
        }
        activity.finish();
    }
}
