package quizapp.icarasia.com.quizapp.LoadingDialog;

import android.app.Activity;
import android.app.ProgressDialog;

/**
 * Created by yasirsiddique on 24/09/2016.
 */

//Not being used now
public class LoadingDialog
{
    private static ProgressDialog progressDialog;

    public static void show(Activity activity, String message)
    {
        if (activity == null || activity.isFinishing())
        {
            return;
        }
        dismiss();
        progressDialog = new ProgressDialog(activity);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.setOwnerActivity(activity);
        progressDialog.show();
    }

    public static void dismiss()
    {
        try
        {
            if (progressDialog != null)
            {
                Activity activity = progressDialog.getOwnerActivity();
                if (activity != null && !activity.isFinishing())
                {
                    progressDialog.dismiss();
                }
                progressDialog = null;
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
