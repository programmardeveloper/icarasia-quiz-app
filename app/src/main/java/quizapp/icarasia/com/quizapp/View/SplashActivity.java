package quizapp.icarasia.com.quizapp.View;

import android.content.Intent;
import android.os.Bundle;

import quizapp.icarasia.com.quizapp.Generic.BaseActivity;
import quizapp.icarasia.com.quizapp.Presenter.ActivityPresenters.SplashPresenter;
import quizapp.icarasia.com.quizapp.R;

/**
 * Created by yasirsiddique on 24/09/2016.
 */
public class SplashActivity extends BaseActivity {

    SplashPresenter presenter = new SplashPresenter();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        presenter.proceedToNextScreen(this);
    }

    public void takeToScreen (Class nextActivity)
    {
         /* Create an Intent that will start the Menu-Activity. */
        Intent intent = new Intent(SplashActivity.this, nextActivity);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        SplashActivity.this.startActivity(intent);
    }

    @Override
    protected void onPause() {
        presenter.stopTimer();
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        presenter.stopTimer();
        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        presenter.startTimer();
        super.onResume();
    }
}
