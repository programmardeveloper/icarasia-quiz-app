package quizapp.icarasia.com.quizapp.View;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import quizapp.icarasia.com.quizapp.Generic.BaseActivity;
import quizapp.icarasia.com.quizapp.Presenter.ActivityPresenters.EditMobileNumPresenter;
import quizapp.icarasia.com.quizapp.R;
import quizapp.icarasia.com.quizapp.Utils.QuizAppConstants;

/**
 * Created by yasirsiddique on 24/09/2016.
 */
public class EditMobileNumDialogActivity extends BaseActivity {

    private EditText mobileNumber;
    private Button save;
    private TextInputLayout mobileNumTextInput;
    private EditMobileNumPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_dialog);
        this.setFinishOnTouchOutside(false);
        presenter = new EditMobileNumPresenter(this);
        setToolbarVisibility(View.GONE);
        setUpView();
        setUpListener();
        initializeFieldsWithData();
    }


    private void initializeFieldsWithData() {
        mobileNumber.setText(presenter.getMobileNumber());
    }

    private void setUpView() {
        mobileNumber = (EditText) findViewById(R.id.mobileNumber);
        save = (Button) findViewById(R.id.save);
        mobileNumTextInput = (TextInputLayout) findViewById(R.id.mobileNumTextInput);
    }

    private void setUpListener() {
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.actionsSaveButtonClick();
            }
        });
    }

    public void showInvalidMobileNumErr() {
        mobileNumTextInput.setError(QuizAppConstants.InvalidMobileError);
    }

    public void showLoginFailure() {
        mobileNumTextInput.setError("Unable to save the change. Please try again!");
    }

    public String getMobileNumberEntered() {
        return mobileNumber.getText().toString();
    }
}
