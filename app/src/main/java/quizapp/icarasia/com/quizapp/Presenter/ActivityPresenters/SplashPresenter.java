package quizapp.icarasia.com.quizapp.Presenter.ActivityPresenters;

import android.os.Handler;

import quizapp.icarasia.com.quizapp.Service.UserService;
import quizapp.icarasia.com.quizapp.View.LoginSignupActivity;
import quizapp.icarasia.com.quizapp.View.MainScreenActivity;
import quizapp.icarasia.com.quizapp.View.SplashActivity;

/**
 * Created by yasirsiddique on 25/09/2016.
 */
public class SplashPresenter {
    /** Duration of wait **/
    private int Display_Duration = 5000;
    private Handler handler = null;
    private Runnable runnable = null;

    public void proceedToNextScreen (final SplashActivity activity)
    {
        /* New Handler to start the Next Activity
        and close this Splash Screen after 5 seconds.*/
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                checkForScreenToProceed (activity);
            }
        };
        startTimer ();
    }

    private void checkForScreenToProceed (SplashActivity activity)
    {
        Class nextActivity;
        boolean isLoggedInUser = UserService.getInstance().shouldLogin(activity);
        if (isLoggedInUser)
            nextActivity = MainScreenActivity.class;
        else
            nextActivity = LoginSignupActivity.class;

        activity.takeToScreen(nextActivity);
    }

    public void stopTimer ()
    {
        if (handler != null && runnable != null)
           handler.removeCallbacks(runnable);
    }

    public void startTimer ()
    {
        handler.postDelayed(runnable, Display_Duration);
    }
}
