package quizapp.icarasia.com.quizapp.View;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import quizapp.icarasia.com.quizapp.Generic.BaseActivity;
import quizapp.icarasia.com.quizapp.Presenter.ActivityPresenters.MainScreenPresenter;
import quizapp.icarasia.com.quizapp.R;
import quizapp.icarasia.com.quizapp.Utils.QuizAppConstants;

/**
 * Created by yasirsiddique on 24/09/2016.
 */
public class MainScreenActivity extends BaseActivity implements View.OnClickListener {

    private TextView firstName;
    private TextView mobileNum;
    private Button editPhoneNum;
    private Button userType;
    private TextView lastName;
    private View view;
    private MainScreenPresenter presenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);
        setUpView();
        setUpListeners();
        presenter = new MainScreenPresenter(this);
        presenter.checkForUserDetails();
    }

    private void setUpView() {
        firstName = (TextView) findViewById(R.id.firstName);
        lastName = (TextView) findViewById(R.id.lastName);
        mobileNum = (TextView) findViewById(R.id.mobileNum);
        editPhoneNum = (Button) findViewById(R.id.editPhoneNum);
        userType = (Button) findViewById(R.id.userType);
        view = findViewById(R.id.main_content);
    }


    private void setUpListeners() {
        userType.setOnClickListener(this);
        editPhoneNum.setOnClickListener(this);
    }

    public void showUserDetails(String firstname, String lastname, String phoneNumber) {
        firstName.setText(firstname);
        lastName.setText(lastname);
        mobileNum.setText(phoneNumber);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        presenter.checkForNumberChange(requestCode, resultCode, data);

        super.onActivityResult(requestCode, resultCode, data);
    }

    public void changeMobileNumberDisplayed() {
        mobileNum.setText(presenter.getSignedInUserPhoneNumber());
        showNumberChangeSuccessMessage();
    }

    private void showNumberChangeSuccessMessage() {

        Snackbar.make(view, "Number edit successfully", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            moveToLogOutScreen();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void moveToLogOutScreen() {
        Intent intent = new Intent(MainScreenActivity.this, LogoutDialogActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        MainScreenActivity.this.startActivity(intent);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.userType:
                showToastForUserTypeInfo();
                break;

            case R.id.editPhoneNum:
                startDialogAcitivityWithData();
                break;
        }
    }

    private void showToastForUserTypeInfo() {
        Toast.makeText(MainScreenActivity.this, "Your account type is " + presenter.getSignedInUserType(), Toast.LENGTH_SHORT).show();
    }

    private void startDialogAcitivityWithData() {
        Intent intent = new Intent(MainScreenActivity.this, EditMobileNumDialogActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra(QuizAppConstants.MobileNumBundleKey, presenter.getSignedInUserPhoneNumber());
        MainScreenActivity.this.startActivityForResult(intent, QuizAppConstants.DialogActivityRequestCode);
    }
}
