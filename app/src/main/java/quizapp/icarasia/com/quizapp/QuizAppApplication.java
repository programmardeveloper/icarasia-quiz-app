package quizapp.icarasia.com.quizapp;

import android.app.Application;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by yasirsiddique on 24/09/2016.
 */
public class QuizAppApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        setUpRealm();
    }

    protected void setUpRealm()
    {
        RealmConfiguration.Builder builder = new RealmConfiguration.Builder(this).schemaVersion(100);
        builder.deleteRealmIfMigrationNeeded();

        RealmConfiguration config = builder.build();
        Realm.setDefaultConfiguration(config);
    }

}
