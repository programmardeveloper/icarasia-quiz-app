package quizapp.icarasia.com.quizapp.SignUp;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import quizapp.icarasia.com.quizapp.Generic.BaseFragment;
import quizapp.icarasia.com.quizapp.R;
import quizapp.icarasia.com.quizapp.Utils.NothingSelectedSpinnerAdapter;
import quizapp.icarasia.com.quizapp.Utils.QuizAppConstants;

/**
 * Created by yasirsiddique on 24/09/2016.
 */
public class SignUpFragment extends BaseFragment {

    EditText firstName;
    EditText lastName;
    EditText emailAddress;
    EditText password;
    EditText phoneNumber;
    Button signUpButton;
    Spinner spinner;
    String userType = "";
    View view;
    TextInputLayout phoneNumberTextInput;
    TextInputLayout emailTextInput;
    TextInputLayout passwordTextInput;
    SignUpPresenter presenter;

    @Override
    public String getTitle() {
        return "";
    }

    @Override
    public void updateFragmentView() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_sign_up, container, false);
        userType = "";
        presenter = new SignUpPresenter(this);
        setUpView();
        addWatcherToPhoneNum();
        setUpListeners();
        setUpDropDown();

        return view;
    }

    private void setUpListeners() {
        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.signUpButtonClicked();
            }
        });
    }

    protected void setUpView() {
        firstName = (EditText) view.findViewById(R.id.firstName);
        lastName = (EditText) view.findViewById(R.id.lastName);
        emailAddress = (EditText) view.findViewById(R.id.emailAddress);
        password = (EditText) view.findViewById(R.id.password);
        phoneNumber = (EditText) view.findViewById(R.id.phoneNumber);
        spinner = (Spinner) view.findViewById(R.id.spinner1);
        signUpButton = (Button) view.findViewById(R.id.signUpButton);

        phoneNumberTextInput = (TextInputLayout) view.findViewById(R.id.phoneNumberTextInput);
        emailTextInput = (TextInputLayout) view.findViewById(R.id.emailAddressTextInput);
        passwordTextInput = (TextInputLayout) view.findViewById(R.id.passwordTextInput);

    }

    private void setUpDropDown() {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(), R.array.user_type_arrays, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(
                new NothingSelectedSpinnerAdapter(
                        adapter,
                        R.layout.contact_spinner_row_nothing_selected,
                        getActivity()));


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                userType = (String) parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });

    }


    protected void addWatcherToPhoneNum() {
        phoneNumber.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
    }


    public String getEnteredUserType() {
        return userType;
    }

    public String getEnteredPassword() {
        return this.password.getText().toString();
    }

    public String getEnteredEmail() {
        return this.emailAddress.getText().toString();
    }

    public String getEnteredPhoneNum() {
        return this.phoneNumber.getText().toString();
    }

    public String getEnteredFirstName() {
        return firstName.getText().toString();
    }

    public String getEnteredLastName() {
        return lastName.getText().toString();
    }

    public void setInvalidEmailErr() {
        emailTextInput.setError(QuizAppConstants.InvalidEmailError);
    }


    public void setInvalidPasswordErr() {
        passwordTextInput.setError(QuizAppConstants.InvalidPasswordError);
    }

    public void setInvalidPhoneNumErr() {
        phoneNumberTextInput.setError(QuizAppConstants.InvalidMobileError);
    }

    public void showUserTypeSelectionErr() {
        TextView errorText = (TextView) spinner.getSelectedView();
        errorText.setTextColor(Color.RED);
        errorText.setText("Please select user type");
    }

    public void showSnackBarForFormCompletion() {
        Snackbar.make(view, "Please complete the form", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }

    public void showLoginFailure() {
        Snackbar.make(view, "Email already exist. Please use 'Login' Tab to log in!", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }

    public void clearAllErrors()
    {
        emailTextInput.setError("");
        passwordTextInput.setError("");
        phoneNumberTextInput.setError("");
    }
}
