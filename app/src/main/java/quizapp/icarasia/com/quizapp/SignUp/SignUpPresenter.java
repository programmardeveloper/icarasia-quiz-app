package quizapp.icarasia.com.quizapp.SignUp;

import quizapp.icarasia.com.quizapp.Factory.UserFactory;
import quizapp.icarasia.com.quizapp.Generic.FormValidator;
import quizapp.icarasia.com.quizapp.Model.BasicUser;
import quizapp.icarasia.com.quizapp.Service.UserService;
import quizapp.icarasia.com.quizapp.Utils.QuizAppConstants;
import quizapp.icarasia.com.quizapp.Utils.QuizAppUIUtils;
import quizapp.icarasia.com.quizapp.View.LoginSignupActivity;

/**
 * Created by yasirsiddique on 25/09/2016.
 */
public class SignUpPresenter {


    SignUpFragment fragment;

    public SignUpPresenter (SignUpFragment fragment)
    {
        this.fragment = fragment;
    }
    private boolean verifyAllFields() {
        boolean fieldsFilled = allFieldsFilled();
        boolean fieldsValidated = validateFields();

        return (fieldsFilled && fieldsValidated);
    }

    private boolean validateFields()
    {
        boolean validEmail = validateEmail();
        boolean validPassword = validatePassword();
        boolean validPhoneNumber = validatePhoneNumber();
        boolean validUserType = validateUserType();
        boolean areAllFieldsValid = validEmail && validPassword && validPhoneNumber && validUserType;
        return areAllFieldsValid;
    }

    private boolean validateUserType ()
    {
        String userType = fragment.getEnteredUserType();
        boolean isValid = (userType != null && !userType.isEmpty());
        if (!isValid)
        {
            fragment.showUserTypeSelectionErr();
        }

        return isValid;
    }

    private boolean validateEmail()
    {
        String emailId = fragment.getEnteredEmail();
        boolean validEmail = FormValidator.isValidEmail(emailId);
        if (!validEmail)
        {
            fragment.setInvalidEmailErr();
        }
        return validEmail;
    }

    private boolean validatePassword()
    {
        String pass = fragment.getEnteredPassword();
        boolean validPassword = FormValidator.isValidPassword(pass);
        if (!validPassword)
        {
            fragment.setInvalidPasswordErr();
        }
        return validPassword;
    }

    private boolean validatePhoneNumber()
    {
        String phoneNumber = fragment.getEnteredPhoneNum();
        boolean validPhoneNumber = phoneNumber.isEmpty() ? true : FormValidator.isValidCellPhone(phoneNumber, QuizAppConstants.PhoneNumberLength);
        if (!validPhoneNumber)
        {
            fragment.setInvalidPhoneNumErr();
        }
        return validPhoneNumber;
    }

    private BasicUser getSignedUpUser ()
    {
        return UserFactory.getUser(fragment.getEnteredUserType(), fragment.getEnteredFirstName(), fragment.getEnteredLastName(), fragment.getEnteredPhoneNum(), fragment.getEnteredEmail(), fragment.getEnteredPassword());
    }

    public void signUpButtonClicked()
    {
        fragment.clearAllErrors();

        if (verifyAllFields ())
        {
            QuizAppUIUtils.hideSoftKeyboard(fragment.getActivity());

            BasicUser user = getSignedUpUser();

            if (!UserService.getInstance().userAlreadyExists(user))
            {
                boolean isSignUpSuccess = UserService.getInstance().isSignUpSuccessful(fragment.getActivity(), user);
                if (isSignUpSuccess)
                    ((LoginSignupActivity)fragment.getActivity()).moveToNextScreen();
            }
            else
            {
                fragment.showLoginFailure();
            }
        }
    }


    private boolean allFieldsFilled ()
    {
        boolean fieldsFilled = FormValidator.isFieldFilled(fragment.getEnteredEmail()) && FormValidator.isFieldFilled(fragment.getEnteredPassword())
                && FormValidator.isFieldFilled(fragment.getEnteredPhoneNum()) && (fragment.getEnteredUserType() != null && !fragment.getEnteredUserType().isEmpty());
        if (!fieldsFilled)
        {
            fragment.showSnackBarForFormCompletion();
        }
        return fieldsFilled;
    }
}
