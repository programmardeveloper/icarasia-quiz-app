package quizapp.icarasia.com.quizapp.Login;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import quizapp.icarasia.com.quizapp.Generic.BaseFragment;
import quizapp.icarasia.com.quizapp.View.LoginSignupActivity;
import quizapp.icarasia.com.quizapp.R;
import quizapp.icarasia.com.quizapp.Utils.QuizAppConstants;

/**
 * Created by yasirsiddique on 24/09/2016.
 */
public class LoginFragment extends BaseFragment {

    EditText emailAddress;
    EditText password;
    Button logInButton;
    View view;
    private TextInputLayout emailTextInput;
    private TextInputLayout passwordTextInput;

    LoginPresenter presenter = null;

    @Override
    public String getTitle() {
        return "Login";
    }

    @Override
    public void updateFragmentView() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_login, container, false);
        if (presenter == null) {
            presenter = new LoginPresenter(this);
        }
        setUpViewVariables(rootView);
        setUpListeners();

        return rootView;
    }

    private void setUpViewVariables(View rootView) {
        emailAddress = (EditText) rootView.findViewById(R.id.emailAddress);
        password = (EditText) rootView.findViewById(R.id.password);
        logInButton = (Button) rootView.findViewById(R.id.loginButton);

        emailTextInput = (TextInputLayout) rootView.findViewById(R.id.emailTextInput);
        passwordTextInput = (TextInputLayout) rootView.findViewById(R.id.passwordTextInput);
        view = rootView;
    }

    private void setUpListeners() {
        logInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onLoginButtonClick();
            }
        });
    }

    public void showLoginFailure() {
        Snackbar.make(view, "Email or Password is incorrect. Please try again!", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }

    public String getEmailEntered() {
        return emailAddress.getText().toString();
    }

    public String getPasswordEntered() {
        return password.getText().toString();
    }

    public void showInvalidEmailErr() {
        emailTextInput.setError(QuizAppConstants.InvalidEmailError);
    }

    public void showInvalidPasswordErr() {
        passwordTextInput.setError(QuizAppConstants.InvalidPasswordError);
    }

    public void showSnackBarForFormCompletion() {
        Snackbar.make(view, "Please complete the form", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }

    public void moveToNextScreen() {
        ((LoginSignupActivity) getActivity()).moveToNextScreen();
    }

    public void clearAllErrors ()
    {
        emailTextInput.setError("");
        passwordTextInput.setError("");
    }
}
