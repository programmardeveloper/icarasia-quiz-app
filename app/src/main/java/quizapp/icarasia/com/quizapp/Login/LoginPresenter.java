package quizapp.icarasia.com.quizapp.Login;

import quizapp.icarasia.com.quizapp.Generic.FormValidator;
import quizapp.icarasia.com.quizapp.Service.UserService;
import quizapp.icarasia.com.quizapp.Utils.QuizAppUIUtils;

/**
 * Created by yasirsiddique on 25/09/2016.
 */
public class LoginPresenter {

    LoginFragment fragment;
    UserService userService;

    public LoginPresenter(LoginFragment fragment) {
        this(fragment, UserService.getInstance());
    }

    public LoginPresenter(LoginFragment fragment, UserService userService) {
        this.fragment = fragment;
        this.userService = userService;
    }

    private boolean verifyAllFields() {
        return allFieldsFilled() && validateFields();
    }

    private boolean shouldAllowLogin() {
        final String email = fragment.getEmailEntered();
        final String pass = fragment.getPasswordEntered();

        boolean allowLogin = userService.shouldLoginWithCredentials(fragment.getActivity(), email, pass);

        return allowLogin;
    }

    public void onLoginButtonClick() {
        fragment.clearAllErrors();
        if (verifyAllFields()) {
            QuizAppUIUtils.hideSoftKeyboard(fragment.getActivity());
            performLoginAction();
        }
    }

    private void performLoginAction() {
        boolean allowLogin = shouldAllowLogin();

        if (allowLogin) {
            fragment.moveToNextScreen();
        } else {
            fragment.showLoginFailure();
        }

    }

    private boolean validateFields() {
        boolean validEmailAddress = validateEmail();
        boolean validPassword = validatePassword();
        return validEmailAddress && validPassword;
    }

    private boolean validateEmail() {
        boolean validEmail = isValidEmailAddress();
        if (!validEmail) {
            fragment.showInvalidEmailErr();
        }
        return validEmail;
    }

    private boolean validatePassword() {
        boolean validPassword = isValidPassword();
        if (!validPassword) {
            fragment.showInvalidPasswordErr();
        }
        return validPassword;
    }

    private boolean isValidEmailAddress() {
        return FormValidator.isValidEmail(fragment.getEmailEntered());

    }

    private boolean isValidPassword() {
        return FormValidator.isValidPassword(fragment.getPasswordEntered());
    }

    private boolean allFieldsFilled() {
        boolean fieldsFilled = FormValidator.isFieldFilled(fragment.getEmailEntered()) && FormValidator.isFieldFilled(fragment.getPasswordEntered());
        if (!fieldsFilled) {
            fragment.showSnackBarForFormCompletion();
        }

        return fieldsFilled;
    }

}
