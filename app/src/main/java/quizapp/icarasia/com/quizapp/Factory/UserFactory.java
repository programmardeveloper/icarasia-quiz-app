package quizapp.icarasia.com.quizapp.Factory;

import quizapp.icarasia.com.quizapp.Model.AgentUser;
import quizapp.icarasia.com.quizapp.Model.BasicUser;
import quizapp.icarasia.com.quizapp.Model.BrokerUser;
import quizapp.icarasia.com.quizapp.Model.DealerUser;
import quizapp.icarasia.com.quizapp.Model.PrivateUser;
import quizapp.icarasia.com.quizapp.Model.User;

/**
 * Created by yasirsiddique on 26/09/2016.
 */
public class UserFactory {

    public static BasicUser getUser (String userType, String fName, String lName, String pNum, String eId, String pass)
    {
        switch (userType)
        {
            case "Private":
                return new PrivateUser(fName, lName, pNum, eId, pass);
            case "Broker":
                return new BrokerUser(fName, lName, pNum, eId, pass);
            case "Agent":
                return new AgentUser(fName, lName, pNum, eId, pass);
            case "Dealer":
                return new DealerUser(fName, lName, pNum, eId, pass);
        }
        return null;
    }


    public static BasicUser getUser (User user)
    {
        if (user != null)
        {
            switch (user.getUserType())
            {
                case "Private":
                    return new PrivateUser(user);
                case "Broker":
                    return new BrokerUser(user);
                case "Agent":
                    return new AgentUser(user);
                case "Dealer":
                    return new DealerUser(user);
            }
        }
        return null;
    }
}
