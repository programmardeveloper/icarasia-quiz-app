package quizapp.icarasia.com.quizapp.Utils;

/**
 * Created by yasirsiddique on 24/09/2016.
 */
public class QuizAppConstants {
    public static final int MinPasswordLength = 8;
    public static final int PhoneNumberLength = 10;//TODO:Check



    public static final String InvalidPasswordError = "Password should contain one special character and minimum "+MinPasswordLength+" character required";
    public static final String InvalidEmailError = "Email is not valid.";
    public static final String InvalidMobileError = "Mobile number is not valid";

    public static final String MobileNumResultKey = "newMobileNumber";
    public static final String MobileNumBundleKey = "mobileNum";

    public static final int DialogActivityRequestCode = 2145;
}
