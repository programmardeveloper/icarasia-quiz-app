package quizapp.icarasia.com.quizapp.View;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import quizapp.icarasia.com.quizapp.Generic.BaseActivity;
import quizapp.icarasia.com.quizapp.Presenter.ActivityPresenters.LogoutPresenter;
import quizapp.icarasia.com.quizapp.R;

/**
 * Created by yasirsiddique on 25/09/2016.
 */
public class LogoutDialogActivity extends BaseActivity {
    private LogoutPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_logout_dialog);
        this.setFinishOnTouchOutside(false);

        setToolbarVisibility(View.GONE);

        presenter = new LogoutPresenter(this);
        presenter.proceedWithLogOut();
    }

    /** To disable back button on this dialog
     *
     */
    @Override
    public void onBackPressed() {

    }

    @Override
    protected void onPause() {
        presenter.stopTimer();
        super.onPause();
    }

    @Override
    protected void onResume() {
        presenter.startTimer();
        super.onResume();
    }

    public void moveToSplashScreen ()
    {
        Intent intent = new Intent(LogoutDialogActivity.this, SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        LogoutDialogActivity.this.startActivity(intent);
    }
}
