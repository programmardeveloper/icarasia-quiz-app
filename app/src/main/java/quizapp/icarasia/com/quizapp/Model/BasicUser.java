package quizapp.icarasia.com.quizapp.Model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by yasirsiddique on 26/09/2016.
 */

public abstract class BasicUser {
    public BasicUser ()
    {

    }

    public BasicUser (User user)
    {
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.phoneNumber = user.getPhoneNumber();
        this.userType = user.getUserType();
        this.emailAddress = user.getEmailAddress();
        this.password = user.getPassword();
    }

    public BasicUser(String firstName, String lastName, String phoneNumber, String emailAddress, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.userType = getUserType();
        this.emailAddress = emailAddress;
        this.password = password;
    }

    public BasicUser(String phoneNumber, String emailAddress, String password) {
        this.phoneNumber = phoneNumber;
        this.userType = getUserType();
        this.emailAddress = emailAddress;
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public abstract String getUserType();
//    public String getUserType() {
//        return userType;
//    }

//    public void setUserType(String userType) {
//        this.userType = userType;
//    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    private String firstName = "";
    private String lastName = "";
    private String phoneNumber;
    private String userType;
    private String password;
    private String emailAddress;

}

