package quizapp.icarasia.com.quizapp.Model;

/**
 * Created by yasirsiddique on 26/09/2016.
 */
public class BrokerUser extends BasicUser {

    public BrokerUser ()
    {

    }

    @Override
    public String getUserType() {
        return "Broker";
    }

    public BrokerUser (User user)
    {
        super(user);
    }

    public BrokerUser(String firstName, String lastName, String phoneNumber, String emailAddress, String password) {
        super(firstName, lastName, phoneNumber, emailAddress, password);
    }
}
