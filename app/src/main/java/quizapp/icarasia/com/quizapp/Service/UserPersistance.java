package quizapp.icarasia.com.quizapp.Service;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import quizapp.icarasia.com.quizapp.Model.BasicUser;
import quizapp.icarasia.com.quizapp.Model.User;

/**
 * Created by yasirsiddique on 24/09/2016.
 */
public class UserPersistance {

    private static UserPersistance userPersistance = null;

    public static UserPersistance getInstance ()
    {
        if (userPersistance == null)
            userPersistance = new UserPersistance();

        return userPersistance;
    }
    public void createUser (BasicUser user)
    {
        Realm realm = Realm.getDefaultInstance();

        // All writes are wrapped in a transaction
        // to facilitate safe multi threading
        realm.beginTransaction();

        // Add a user
        User person = realm.createObject(User.class);
        person.setFirstName(user.getFirstName());
        person.setLastName(user.getLastName());
        person.setPhoneNumber(user.getPhoneNumber());
        person.setEmailAddress(user.getEmailAddress());
        person.setUserType(user.getUserType());
        person.setPassword(user.getPassword());

        realm.commitTransaction();
    }

    public void updatePhoneNum (final String emailId, final String mobileNumber)
    {
        Realm realm = Realm.getDefaultInstance();

        User user = realm.where(User.class)
                .equalTo("emailAddress", emailId).findFirst();

        realm.beginTransaction();
        user.setPhoneNumber(mobileNumber);
        realm.commitTransaction();
    }

    public User searchUser (String email)
    {
        Realm realm = Realm.getDefaultInstance();

        // Build the query looking at all users:
        RealmQuery<User> query = realm.where(User.class);

        // Add query conditions:
        query.equalTo("emailAddress", email);

        // Execute the query:
        RealmResults<User> result1 = query.findAll();

        if (result1 != null &&  !result1.isEmpty())
            return result1.get(0);

        return null;
    }

    public User verifyLoginCredentials (String email, String password)
    {
        Realm realm = Realm.getDefaultInstance();

        // Build the query looking at all users:
        RealmQuery<User> query = realm.where(User.class);

        // Add query conditions:
        query.equalTo("emailAddress", email);
        query.equalTo("password", password);

        // Execute the query:
        RealmResults<User> result1 = query.findAll();

        if (result1 != null &&  !result1.isEmpty())
        {
            return result1.get(0);
        }

        return null;
    }
}
