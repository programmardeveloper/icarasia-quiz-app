package quizapp.icarasia.com.quizapp.Presenter.ActivityPresenters;

import android.app.Activity;
import android.content.Intent;

import quizapp.icarasia.com.quizapp.Model.BasicUser;
import quizapp.icarasia.com.quizapp.Service.UserService;
import quizapp.icarasia.com.quizapp.Utils.QuizAppConstants;
import quizapp.icarasia.com.quizapp.View.MainScreenActivity;

/**
 * Created by yasirsiddique on 25/09/2016.
 */
public class MainScreenPresenter {

    BasicUser user = null;
    MainScreenActivity activity;

    public MainScreenPresenter (MainScreenActivity activity)
    {
        this.activity = activity;
    }
    public void checkForUserDetails ()
    {
        user = getSignedInUser();

        if (user != null)
            activity.showUserDetails(user.getFirstName(), user.getLastName(), user.getPhoneNumber());
    }


    private BasicUser getSignedInUser() {
        return UserService.getInstance().getLoggedInUser();
    }

    public String getSignedInUserType ()
    {
        return getSignedInUser().getUserType();
    }

    public String getSignedInUserPhoneNumber ()
    {
        return getSignedInUser().getPhoneNumber();
    }

    public void checkForNumberChange (int requestCode, int resultCode, Intent data)
    {
        if (requestCode == QuizAppConstants.DialogActivityRequestCode && resultCode == Activity.RESULT_OK) {
            boolean isNumChanged = data.getBooleanExtra(QuizAppConstants.MobileNumResultKey, false);
            if (isNumChanged) {
                activity.changeMobileNumberDisplayed();
            }
        }
    }
}
