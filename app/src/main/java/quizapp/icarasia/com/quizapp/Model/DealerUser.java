package quizapp.icarasia.com.quizapp.Model;

/**
 * Created by yasirsiddique on 26/09/2016.
 */
public class DealerUser extends BasicUser {

    public DealerUser ()
    {

    }

    @Override
    public String getUserType() {
        return "Dealer";
    }

    public DealerUser (User user)
    {
        super(user);
    }

    public DealerUser(String firstName, String lastName, String phoneNumber, String emailAddress, String password) {
        super(firstName, lastName, phoneNumber, emailAddress, password);
    }
}
