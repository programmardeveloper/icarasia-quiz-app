package quizapp.icarasia.com.quizapp.Service;

import android.app.Activity;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by yasirsiddique on 25/09/2016.
 */
public class UserSession {
    private static UserSession userSession = null;

    public static UserSession getInstance ()
    {
        if (userSession == null)
            userSession = new UserSession();

        return userSession;
    }

    public void saveUserSession (Activity activity, String emailId)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(activity.getApplicationContext());
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("userId", emailId);
        editor.commit();
    }

    public void clearUserSession (Activity activity)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(activity.getApplicationContext());
        SharedPreferences.Editor editor = settings.edit();
        editor.remove("userId");
        editor.commit();
    }

    public boolean isUserLoggedIn (Activity activity)
    {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(activity.getApplicationContext());
        String username = prefs.getString("userId", "");

        return (!username.isEmpty());
    }

    public String getLoggedInUserEmailId (Activity activity)
    {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(activity.getApplicationContext());
        String username = prefs.getString("userId","");

        return username;
    }
}
