package quizapp.icarasia.com.quizapp.Generic;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.LayoutRes;
import android.support.v4.app.NavUtils;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import quizapp.icarasia.com.quizapp.R;
import quizapp.icarasia.com.quizapp.Utils.QuizAppUIUtils;

/**
 * Created by yasirsiddique on 24/09/2016.
 */

public class BaseActivity extends AppCompatActivity
{
    protected Toolbar toolbar;

    @Override
    public void setContentView(@LayoutRes int layoutResID)
    {
        super.setContentView(layoutResID);
        setUpToolBar();
    }

    protected void setUpToolBar()
    {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null)
        {
            setSupportActionBar(toolbar);
            enableBackButton(true);
            setCrossButtonIfRequired();
        }
    }

    public void enableBackButton(boolean isEnabled)
    {
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null)
        {
            supportActionBar.setDisplayHomeAsUpEnabled(isEnabled);
            supportActionBar.setDisplayShowHomeEnabled(isEnabled);
        }
    }

    public void setTitle(String title, int color)
    {
        if (toolbar != null && title != null)
        {
            TextView textView = (TextView) toolbar.findViewById(R.id.title);
            if (textView != null)
            {
                textView.setText(Html.fromHtml(title));
                textView.setTextColor(ContextCompat.getColor(this, color));
            }
        }
    }

    protected void setToolbarColor(int toolbarColor)
    {
        if (toolbar != null)
        {
            toolbar.setBackgroundColor(ContextCompat.getColor(this, toolbarColor));
        }
    }

    public void setTitle(String title)
    {
        setTitle(title, getToolBarTitleColor());
    }

    public void setCrossButtonIfRequired()
    {
        if(showCrossButtonForBackNavigation() && toolbar != null)
        {
//            toolbar.setNavigationIcon(R.drawable.cancel_btn);
        }
    }

    public void setToolbarVisibility(int visibility)
    {
        View toolbarContainer = findViewById(R.id.toolbarContainer);
        if(toolbarContainer != null)
        {
            toolbarContainer.setVisibility(visibility);
        }
    }
    private int getToolBarTitleColor()
    {
        return R.color.toolbarTitle;
    }

    @Override
    public void onBackPressed()
    {
        navigateUp();
    }

    protected void navigateUp()
    {
        Intent intent = NavUtils.getParentActivityIntent(this);
        if (intent != null)
        {
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        }
        QuizAppUIUtils.hideSoftKeyboard(this);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem)
    {
        if (menuItem.getItemId() == android.R.id.home)
        {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(menuItem);
    }

    protected boolean showCrossButtonForBackNavigation()
    {
        return false;
    }

}
