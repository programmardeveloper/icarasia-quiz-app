package quizapp.icarasia.com.quizapp.Model;

/**
 * Created by yasirsiddique on 26/09/2016.
 */
public class AgentUser extends BasicUser {

    public AgentUser ()
    {

    }

    @Override
    public String getUserType() {
        return "Agent";
    }

    public AgentUser (User user)
    {
        super(user);
    }

    public AgentUser(String firstName, String lastName, String phoneNumber, String emailAddress, String password) {
        super(firstName, lastName, phoneNumber, emailAddress, password);
    }
}