package quizapp.icarasia.com.quizapp.Service;

import android.app.Activity;

import quizapp.icarasia.com.quizapp.Factory.UserFactory;
import quizapp.icarasia.com.quizapp.Model.BasicUser;
import quizapp.icarasia.com.quizapp.Model.User;

/**
 * Created by yasirsiddique on 25/09/2016.
 */
public class UserService {

    BasicUser currentUser = null;
    private static UserService userService = null;

    public static UserService getInstance ()
    {
        if (userService == null)
            userService = new UserService();

        return userService;
    }

    public boolean shouldLogin (Activity activity)
    {
        boolean wasAnyUserLoggedIn = UserSession.getInstance().isUserLoggedIn(activity);
        if (wasAnyUserLoggedIn)
        {
            String loggedUserEmailId = UserSession.getInstance().getLoggedInUserEmailId(activity);
            User user = UserPersistance.getInstance().searchUser(loggedUserEmailId);
            if (user != null)
            {
                currentUser = UserFactory.getUser(user);
            }
        }

        return (currentUser != null);
    }

    public BasicUser getLoggedInUser ()
    {
        return currentUser;
    }

    public void clearCurrentSession (Activity activity)
    {
        UserSession.getInstance().clearUserSession(activity);
        currentUser = null;
    }

    public boolean shouldLoginWithCredentials (Activity activity, String email, String password)
    {
        User user = UserPersistance.getInstance().verifyLoginCredentials(email, password);
        if (user != null)
        {
            currentUser = UserFactory.getUser(user);
        }

        if (currentUser != null)
            UserSession.getInstance().saveUserSession(activity, email);

        return (currentUser != null);
    }

    public boolean isSignUpSuccessful (Activity activity, BasicUser user)
    {
        try
        {
            UserPersistance.getInstance().createUser(user);
            UserSession.getInstance().saveUserSession(activity, user.getEmailAddress());
            currentUser = user;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return currentUser != null;
    }

    public boolean userAlreadyExists (BasicUser user)
    {
        User existingUser = UserPersistance.getInstance().searchUser(user.getEmailAddress());

        return (existingUser != null);
    }

    public boolean updateUser (String mobileNumber)
    {
        try {
            UserPersistance.getInstance().updatePhoneNum(currentUser.getEmailAddress(), mobileNumber);
            currentUser.setPhoneNumber(mobileNumber);
            return true;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return false;
        }
    }
}
