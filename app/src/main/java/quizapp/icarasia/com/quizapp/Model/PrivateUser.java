package quizapp.icarasia.com.quizapp.Model;

/**
 * Created by yasirsiddique on 26/09/2016.
 */
public class PrivateUser extends BasicUser {

    public PrivateUser ()
    {

    }

    @Override
    public String getUserType() {
        return "Private";
    }

    public PrivateUser (User user)
    {
        super(user);
    }

    public PrivateUser(String firstName, String lastName, String phoneNumber, String emailAddress, String password) {
        super(firstName, lastName, phoneNumber, emailAddress, password);
    }
}
