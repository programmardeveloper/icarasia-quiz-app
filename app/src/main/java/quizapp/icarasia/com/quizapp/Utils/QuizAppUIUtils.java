package quizapp.icarasia.com.quizapp.Utils;

import android.app.Activity;
import android.view.inputmethod.InputMethodManager;

/**
 * Created by yasirsiddique on 24/09/2016.
 */
public class QuizAppUIUtils {

    public static void hideSoftKeyboard(Activity activity)
    {
        try
        {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
