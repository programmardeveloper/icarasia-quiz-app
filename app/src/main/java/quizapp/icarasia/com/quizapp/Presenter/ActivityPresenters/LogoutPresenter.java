package quizapp.icarasia.com.quizapp.Presenter.ActivityPresenters;

import android.os.Handler;

import quizapp.icarasia.com.quizapp.Service.UserService;
import quizapp.icarasia.com.quizapp.View.LogoutDialogActivity;

/**
 * Created by yasirsiddique on 25/09/2016.
 */
public class LogoutPresenter {

    /** Duration of wait **/
    private final int Display_Duration = 5000;
    private LogoutDialogActivity activity;
    private Handler handler = null;
    private Runnable runnable = null;

    public LogoutPresenter (LogoutDialogActivity activity)
    {
        this.activity = activity;
    }

    public void proceedWithLogOut ()
    {
        /* New Handler to start the Next Activity
        and close this Splash Screen after 5 seconds.*/

        handler = new Handler();
        runnable = new Runnable(){
            @Override
            public void run() {

                logOutUser();

            }
        };
        startTimer ();
    }

    private void logOutUser ()
    {
        UserService.getInstance().clearCurrentSession(activity);
        activity.moveToSplashScreen();
    }

    public void stopTimer ()
    {
        if (handler != null && runnable != null)
            handler.removeCallbacks(runnable);
    }

    public void startTimer ()
    {
        handler.postDelayed(runnable, Display_Duration);
    }

}
