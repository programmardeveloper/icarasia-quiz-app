package quizapp.icarasia.com.quizapp.Generic;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import java.util.List;

import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import quizapp.icarasia.com.quizapp.Utils.QuizAppUIUtils;

/**
 * Created by yasirsiddique on 24/09/2016.
 */

public abstract class BaseFragment extends Fragment
{
    protected View view;

    public abstract String getTitle();

    public boolean onBackPressed()
    {

        FragmentManager supportFragmentManager = getChildFragmentManager();
        int childCount = supportFragmentManager.getBackStackEntryCount();
        if (childCount == 0)
        {
            // it has no child Fragment
            // can not handle the onBackPressed task by itself
            return false;
        }
        else
        {
            // get the child Fragment
            List<Fragment> childFragments = supportFragmentManager.getFragments();
            int size = childFragments.size();
            if (size > 0)
            {
                Fragment fragment = childFragments.get(size - 1);
                if (fragment instanceof BaseFragment)
                {
                    BaseFragment childFragment = (BaseFragment) fragment;
                    // propagate onBackPressed method call to the child Fragment
                    if (!childFragment.onBackPressed())
                    {
                        // child Fragment was unable to handle the task
                        // It could happen when the child Fragment is last last leaf of a chain
                        // removing the child Fragment from stack
                        supportFragmentManager.popBackStackImmediate();
                        updateFragmentView();
                    }
                    // either this Fragment or its child handled the task
                    // either way we are successful and done here
                    return true;
                }
            }
            else
            {
                return false;
            }
        }
        return false;
    }

    /**
     * Update fragment view that is currently visible to user.
     */
    public abstract void updateFragmentView();

    @Override
    public void onDestroyView()
    {
        if (view != null)
        {
            ViewParent parent = view.getParent();
            if (parent != null)
            {
                ((ViewGroup) parent).removeView(view);
            }
        }
        super.onDestroyView();
    }

    @Override
    public void onResume()
    {
        super.onResume();
    }

    @Override
    public void onPause()
    {
        super.onPause();
        QuizAppUIUtils.hideSoftKeyboard(getActivity());
    }
}

