package quizapp.icarasia.com.quizapp.Login;

import android.app.ListFragment;

import org.junit.After;
import org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.robolectric.Robolectric;
import org.robolectric.shadows.support.v4.SupportFragmentTestUtil;
import org.robolectric.util.FragmentTestUtil;

import quizapp.icarasia.com.quizapp.BaseTestRobolectric;

import static org.junit.Assert.*;

/**
 * Created by yasirsiddique on 26/09/2016.
 */

public class LoginFragmentTest extends BaseTestRobolectric {


    @Mock LoginPresenter presenter;

    LoginFragment fragment;


    @Before
    public void setUp() throws Exception {
        fragment = new LoginFragment();
        fragment.presenter = presenter;
        SupportFragmentTestUtil.startFragment(new LoginFragment());
    }

    @Test
    public void test () {
        assertEquals("1 should be equal to", 1, 1);
    }

    @After
    public void tearDown() throws Exception {

    }

}