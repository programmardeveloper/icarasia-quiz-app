package quizapp.icarasia.com.quizapp.Login;

import android.app.Activity;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import quizapp.icarasia.com.quizapp.BaseTestRobolectric;
import quizapp.icarasia.com.quizapp.Service.UserService;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anySet;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by yasirsiddique on 26/09/2016.
 */
public class LoginPresenterTest extends BaseTestRobolectric{

    @Mock LoginFragment fragment;
    @Mock UserService userService;

    LoginPresenter presenter;

    @Before
    public void setup () {
        presenter = new LoginPresenter(fragment, userService);

    }

    @Test
    public void showLoginMessage () {
        when(userService.shouldLoginWithCredentials(any(Activity.class), anyString(), anyString())).thenReturn(true);

        when(fragment.getEmailEntered()).thenReturn("yasir@gmail.com");
        when(fragment.getPasswordEntered()).thenReturn("asdfgh123@#$%^&*");

        presenter.onLoginButtonClick();


        verify(fragment).moveToNextScreen();
    }

    @Test
    public void showPasswordErrorIfPasswordIsNotCorrect () {
        when(userService.shouldLoginWithCredentials(any(Activity.class), anyString(), anyString())).thenReturn(false);

        when(fragment.getEmailEntered()).thenReturn("yasir@gmail.com");
        when(fragment.getPasswordEntered()).thenReturn("1");

        presenter.onLoginButtonClick();

        verify(fragment).showInvalidPasswordErr();

    }
}