package quizapp.icarasia.com.quizapp;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

/**
 * Created by yasirsiddique on 26/09/2016.
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 23)
public class BaseTestRobolectric {

    @Before
    public void initMocks () {
        MockitoAnnotations.initMocks(this);
    }
}
